import React, { Component, useState, useEffect } from "react";
import "./App.css";
import { CardList } from "./components/card-list/card-list.component";
import { SearchBox } from "./components/search-box/search-box.component";

const App = () => {
  const [monsters, setMonsters] = useState([]);
  const [searchField, setsearchField] = useState("");

  useEffect(() => {
    (async () => {
      const users = await fetch(
        "https://jsonplaceholder.typicode.com/users"
      ).then(response => response.json());
      setMonsters(users);
    })();
  }, []);

  const onChange = event => {
    setsearchField(event.target.value);
  };

  const filteredMonsters = monsters.filter(m =>
    m.name.toLowerCase().includes(searchField.toLowerCase())
  );
  return (
    <div className="App">
      <h1>Monster Roloder</h1>
      <SearchBox placeholder="Search monsters" onChange={onChange} />
      <p>{searchField}</p>
      <CardList monsters={filteredMonsters}></CardList>
    </div>
  );
};

// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       monsters: [],
//       searchField: ""
//     };
//   }

//   async componentDidMount() {
//     const users = await fetch(
//       "https://jsonplaceholder.typicode.com/users"
//     ).then(response => response.json());
//     this.setState({ monsters: users });
//   }

//   onChange = e => {
//     this.setState({ searchField: e.target.value });
//   };

//   render() {
//     const { monsters, searchField } = this.state;
//     const filteredMonsters = monsters.filter(m =>
//       m.name.toLowerCase().includes(searchField.toLowerCase())
//     );
//     return (
//       <div className="App">
//         <SearchBox placeholder="Search monsters" onChange={onChange} />
//         <p>{this.state.searchField}</p>
//         <CardList monsters={filteredMonsters}></CardList>
//       </div>
//     );
//   }
// }

export default App;
