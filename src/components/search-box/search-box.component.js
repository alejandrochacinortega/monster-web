import React from "react";

import "./searc-box.styles.css";

export const SearchBox = ({ placeholder, onChange }) => {
  return (
    <input
      className="search"
      placeholder={placeholder}
      type="search"
      onChange={onChange}
    />
  );
};
